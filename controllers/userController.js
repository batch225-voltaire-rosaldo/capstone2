const User = require("../models/user");
const Product = require("../models/product");
//const Order = require("../models/order");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10) 
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return user
		};
	});
}; 

// User Login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null ){
			return {
				message: "User not found in database."
			}
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}
			} else {
				return {
					message: "Password was incorrect."
				}
			};
		};
	});
};

// Check Email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 1) {
			return {
				message: "Duplicate User"
			} 
		} else if (result.length > 0){
			return {
				message: "User was found"
			}
		} else {
			return {
				message: "User not found"
			}
		}
	});
};

// Set User as Admin or just User
module.exports.updateUsertoAdmin = (reqParams) => {
	let updatedUser = {
		isAdmin : true
	};
	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
		if (error) {
			return false;
		} else {
			return {
				message: "User changed to Admin successfully"
			}
		};
	});
};

// Get all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};

// Retrieve User Details
module.exports.getDetail = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// Order
module.exports.order = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.order.push({productId : data.productId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
	});
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.user.push({userId : data.userId});
		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});
	if(isUserUpdated && isProductUpdated){
		return {
			message: "Order Added"
		}
	} else {
		return false;
	};
};

/*
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
	});
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId});
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});
	if(isUserUpdated && isCourseUpdated){
		return {
			message: "You're enrolled"
		}
	} else {
		return false;
	};
};
*/
