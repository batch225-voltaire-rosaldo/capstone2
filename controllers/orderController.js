/*
const User = require("../models/user");
const Course = require("../models/course");
const Product = require("../models/product");
const Order = require("../models/order");

const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 1) {
			return {
				message: "Duplicate User"
			} 
		} else if (result.length > 0){
			return {
				message: "User was found"
			}
		} else {
			return {
				message: "User not found"
			}
		}
	});
};

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10) 
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return user
		};
	});
}; 

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null ){
			return {
				message: "Not found in our database"
			}
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access : auth.createAccessToken(result)}
			} else {
				return {
					message: "password was incorrect"
				}
			};
		};
	});
};

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
	});
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId});
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});
	if(isUserUpdated && isCourseUpdated){
		return {
			message: "You're enrolled"
		}
	} else {
		return false;
	};
};
*/