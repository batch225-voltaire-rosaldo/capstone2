// #region  User Controller
const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController")

// User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});

// User Login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

// Check Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

// Update User to Admin
router.put("/userToadmin/:userId", auth.verify, (req, res) => {
	userController.updateUsertoAdmin(req.params).then(result => res.send(result));
});

// Set User to Admin using post

// Get User details
router.post("/userDetails", auth.verify, (req, res) => {
	userController.getDetail({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
});

//
// Get all users
router.get("/getAllUsers", auth.verify, (req, res) => { //need for middleware
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
});

// User Order
router.post("/order", (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}
	userController.order(data).then(result => res.send(result));
});

/*
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result));
});
*/

module.exports = router;
// #endregion
