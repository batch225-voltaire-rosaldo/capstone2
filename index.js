// #region Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();
// #endregion

// User Routes
const userRoutes = require("./routes/userRoutes");
// Product Routes
const productRoutes = require("./routes/productRoutes");

// Order Routes
//const orderRoutes = require("./routes/orderRoutes");

// #region Connect Express, Mongoose, db connection Mongo DB account
// Connect Express
const app = express();
const port = 3009;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Connect Mongoose 
mongoose.connect(`mongodb+srv://vrosaldonovulutions:${process.env.PASSWORD}@cluster0.qikdkns.mongodb.net/Ecommerce-api?retryWrites=true&w=majority`, 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB."))
// #endregion

// App E-Commerce
app.use("/users", userRoutes);
//app.use("/orders", orderRoutes);
app.use("/product", productRoutes);

// #region Server Port
app.listen(port, () => console.log(`E-Commerce API is now online on port ${port}`));
// #endregion
