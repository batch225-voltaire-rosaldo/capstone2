// #region User Schema
const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
    password : {
		type : String,
		required : [true, "Password is required"]
    },
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile Number is required"]
	},
/*
	enrollments : [
		{
			courseId : {
				type : String,
				required: [true, "Course Id is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date() 
			},
            status : {
                type : String,
                default : "Enrolled"
            }
		}
	],
*/
	order : [
		{
			productId : {
				type : String,
				required: [true, "Product Id is required"]
			},
			productQuantity : {
				type : Number,
				required : [true, "Quantity is required"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			},
			isValid : {
				type : Boolean,
				default : true
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);
// #endregion
